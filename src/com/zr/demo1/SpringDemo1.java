package com.zr.demo1;

import org.junit.jupiter.api.Test;

public class SpringDemo1 {

	@Test
	//JDK动态代理
	public void demo1() {
		UserDao userDao = new UserDaoImpl();
		//创建代理
		UserDao proxy = new JdkProxy(userDao).creatProxy();
		proxy.save();
		proxy.delete();
		proxy.find();
		proxy.update();
		
		//userDao.save();
		//userDao.delete();
		//userDao.find();
		//userDao.update();
	}
}