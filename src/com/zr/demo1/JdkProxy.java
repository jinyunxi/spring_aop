package com.zr.demo1;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * 使用JDK动态代理对UserDao产生代理
 * 
 * @author zhan_rong
 *1.0
 */
public class JdkProxy implements InvocationHandler {
	//构造方法传入UserDao,把被增强的对象传递到代理中
	public UserDao userDao;
	public JdkProxy(UserDao userDao) {
		this.userDao = userDao;		
	}
/**
 * 产生UserDao代理的方法
 * @return
 */
	public UserDao creatProxy() {
		//类加载器，
		UserDao userDaoProxy = (UserDao) Proxy.newProxyInstance(userDao.getClass().getClassLoader(), 
				userDao.getClass().getInterfaces(), this);
		return userDaoProxy;
	}
	@Override
		public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		//判断方法名是不是save
		if("save".equals(method.getName())) {
			//增强，增强，权限校验
			System.out.println("权限校验=========");
			return method.invoke(userDao, args);
		}
		return method.invoke(userDao, args);
	}
}
